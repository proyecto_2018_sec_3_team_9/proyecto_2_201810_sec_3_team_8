package model.data_strucutres.test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.RedBlackBST;

public class RedBlackBSTTest <Key extends Comparable<Key>, Value> 
{
	private RedBlackBST<Integer, Integer> rbBST;

	/**
	 * Creates a new empty RedBlack BST. 
	 */	
	@Before
	public void setup1()
	{
		rbBST = new RedBlackBST<Integer, Integer>();
	}

	/**
	 * Test 1: Verifies method put(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 */
	@Test
	public void testPut()
	{
		//Se llama el metodo que crea un nuevo RBBST.
		setup1();

		rbBST.put(1, 2);
		rbBST.put(2, 3);

		assertEquals(2, rbBST.size());
	}

	/**
	 * Test 2: Verifies method get(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * get<br>
	 */
	@Test
	public void testGet()
	{
		//Se llama el metodo que crea un nuevo RBBST.
		setup1();

		rbBST.put(2, 4);	
		rbBST.put(3, 5);

		assertTrue(rbBST.get(2) == 4);
	}

	/**
	 * Test 3: Verifies method delete(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * delete<br>
	 * size<br>
	 */
	@Test
	public void testDelete()
	{
		//Se llama el metodo que crea un nuevo RBBST.
		setup1();

		rbBST.put(2, 4);
		rbBST.put(3, 5);
		rbBST.delete(2);

		assertEquals(1, rbBST.size());
	}

	/**
	 * Test 5: Verifies method keys(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * keys<br>
	 */
	@Test
	public void testIter()
	{
		//Se llama el metodo que crea un nuevo RBBST.
		setup1();

		rbBST.put(4, 6);
		rbBST.put(5, 7);
		Iterable iter = rbBST.keys();
		Iterator iter2 = iter.iterator();

		assertEquals(4, iter2.next());
		assertEquals(5, iter2.next());
	}
}
