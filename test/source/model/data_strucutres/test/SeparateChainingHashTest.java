package model.data_strucutres.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.SeparateChainingHash;

public class SeparateChainingHashTest<Key, Value> 
{

	private SeparateChainingHash<Integer, Integer> hashTable;

	/**
	 * Creates a new empty Separate Chaining HT. 
	 */
	@Before
	public void setupEscenary1()
	{
		hashTable = new SeparateChainingHash<Integer, Integer>();
	}

	/**
	 * Test 1: Verifies method put(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * */
	@Test
	public void testPut()
	{
		//Se usa el escenario 1 de una SCHT.
		setupEscenary1();
		
		//Se prueba método put
		hashTable.put(13, 11);
		
		assertTrue(hashTable.get(13)==11);
	}

	/**
	 * Test 2: Verifies method get(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * get<br>
	 */
	@Test
	public void testGet()
	{
		//Se usa el escenario 1 de una SCHT.
		setupEscenary1();
		
		//Se agregan pares de llave-valor a la SCHT.
		hashTable.put(20, 18);
		hashTable.put(16, 18);
		
		assertTrue(hashTable.get(20)==18);
		assertTrue(hashTable.get(16)==18);
	}

	/**
	 * Test 3: Verifies method delete(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * delete<br>
	 */
	@Test
	public void testDelete()
	{
		//Se usa el escenario 1 de una SCHT.
		setupEscenary1();
		
		//Se agregan pares de llave-valor a la SCHT para luego eliminar una llave y probar.
		hashTable.put(10, 8);
		hashTable.put(6, 8);
		hashTable.delete(10);
		
		assertTrue(hashTable.get(10)==null);
	}
	
	/**
	 * Test 4: Verifies method resize(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * resize<br>
	 */
	@Test
	public void testRehash()
	{
		//Se usa el escenario 1 de una SCHT.
		setupEscenary1();
		
		//Se agregan pares de llave-valor a la SCHT para luego probar metodo resize().
		hashTable.put(8,18);
		hashTable.put(2,14);
		hashTable.resize(8);
		
		assertTrue(18 == hashTable.get(8));
		assertTrue(11 == hashTable.size());
	}
	
	/**
	 * Test 5: Verifies method keys(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * keys<br>
	 */
	@Test
	public void testIter()
	{
		//Se usa el escenario 1 de una SCHT.
		setupEscenary1();

		hashTable.put(5, 4);
		
		assertEquals(hashTable.get(5), hashTable.keys().iterator().next());
	}
}
