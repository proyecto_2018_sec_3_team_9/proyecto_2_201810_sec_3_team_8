package model.data_strucutres.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.LinearProbingHash;

public class LinearProbingHashTest<Key, Value> 
{
	private LinearProbingHash<Integer, Integer> linearHash;

	/**
	 * Creates a new empty LinearProbing HT. 
	 */
	@Before
	public void setupEscenario1()
	{
		linearHash = new LinearProbingHash<Integer, Integer>();
	}

	/**
	 * Test 1: Verifies method put(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 */
	@Test
	public void testPut()
	{
		//Primer escenario con una linear probing HT
		setupEscenario1();
		
		//Se prueba metodo put().
		linearHash.put(15, 13);
		
		assertTrue(linearHash.get(15)==13);
	}

	/**
	 * Test 2: Verifies method get(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * get<br>
	 */
	@Test
	public void testGet()
	{
		//Primer escenario con una linear probing HT
		setupEscenario1();
		
		//Se agregan pares llave-valor a la LPHT.
		linearHash.put(9, 7);	
		linearHash.put(5, 7);
		
		assertTrue(linearHash.get(9)==7);
		assertTrue(linearHash.get(5)==7);
	}

	/**
	 * Test 3: Verifies method delete(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * delete<br>
	 * get<br>
	 */
	@Test
	public void testDelete()
	{
		//Primer escenario con una linear probing HT
		setupEscenario1();

		//Se agregan pares de llave-valor a la LPHT para luego eliminar una llave y probar.
		linearHash.put(10, 8);
		linearHash.put(6, 8);
		linearHash.delete(10);
		
		assertTrue(linearHash.get(10)==null);
	}

	/**
	 * Test 4: Verifies method resize(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * resize<br>
	 */
	@Test
	public void testReHash()
	{
		//Primer escenario con una linear probing HT
		setupEscenario1();
		
		//Se agregan pares de llave-valor a la SCHT para luego probar metodo resize().
		linearHash.put(8,18);
		linearHash.put(2,14);
		linearHash.resize(8);
		
		assertTrue(18 == linearHash.get(8));
		assertTrue(11 == linearHash.size());
	}

	/**
	 * Test 5: Verifies method keys(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * keys<br>
	 */
	@Test
	public void testIter()
	{
		//Primer escenario con una linear probing HT
		setupEscenario1();

		linearHash.put(5, 4);
		
		assertEquals(linearHash.get(5), linearHash.keys().iterator().next());
	}
}

