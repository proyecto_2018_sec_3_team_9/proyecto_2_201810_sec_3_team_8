package model.data_strucutres.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.BinarySearchTree;

public class BSTTest<Key extends Comparable<Key>, Value> 
{
	private BinarySearchTree<Integer, Integer> bst;

	/**
	 * Creates a new empty BST. 
	 */
	@Before
	public void setup1()
	{
		bst = new BinarySearchTree<Integer, Integer>();
	}
	
	/**
	 * Test 1: Verifies method put(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 */
	@Test
	public void testPut()
	{
		//Se llama el metodo que crea un nuevo BST.
		setup1();

		bst.put(1, 2);
		bst.put(2, 3);

		assertEquals(2, bst.size());
	}

	/**
	 * Test 2: Verifies method get(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * get<br>
	 */
	@Test
	public void testGet()
	{
		//Se llama el metodo que crea un nuevo BST.
		setup1();

		bst.put(2, 4);	
		bst.put(3, 5);

		assertTrue(bst.get(2) == 4);
	}

	/**
	 * Test 3: Verifies method delete(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * delete<br>
	 * size<br>
	 */
	@Test
	public void testDelete()
	{
		//Se llama el metodo que crea un nuevo BST.
		setup1();

		bst.put(2, 4);
		bst.put(3, 5);
		bst.delete(2);

		assertEquals(1, bst.size());
	}

	/**
	 * Test 5: Verifies method keys(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * keys<br>
	 */
	@Test
	public void testIter()
	{
		//Se llama el metodo que crea un nuevo BST.
		setup1();

		bst.put(4, 6);
		bst.put(5, 7);
		Iterable iter = bst.keys();
		Iterator iter2 = iter.iterator();

		assertEquals(4, iter2.next());
		assertEquals(5, iter2.next());
	}
}
