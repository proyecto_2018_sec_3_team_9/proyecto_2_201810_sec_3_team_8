package model.logic;

import java.io.File;



import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.sun.javafx.scene.layout.region.Margins.Converter;

import model.data_structures.BinarySearchTree;
import model.data_structures.HeapSort;
import model.data_structures.ILinkedList;
import api.ITaxiTripsManager;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.data_structures.LinearProbingHash;
import model.data_structures.LinkedList;
import model.data_structures.MergeSort;
import model.data_structures.Node;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHash;
import model.data_structures.ServiceAreaComparator;
import model.data_structures.ServiceCompanyComparator;
import model.data_structures.ServiceDateComparator;
import model.data_structures.ServiceDistanceComparator;
import model.data_structures.ServicePickUpDropOffAreaComparator;
import model.data_structures.ServiceTaxiIdComparator;
import model.data_structures.TaxiIdComparator;
import model.data_structures.TaxiPointsComparator;
import model.vo.FechaServicios;
import model.vo.RangoDuracion;
import model.vo.Servicio;
import model.vo.Taxi;


public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";

	private  RedBlackBST<String, LinkedList<String>> treeA1;
	private LinkedList<RangoDuracion> rangos;
	private SeparateChainingHash <Integer, LinkedList<Servicio>> hashA2;
	private SeparateChainingHash <Integer, LinkedList<Servicio>> hashDistancias;
	private SeparateChainingHash<Integer, LinkedList<Servicio>> hash;
	private BinarySearchTree<Double, LinkedList<Servicio>> treeB1;
	private SeparateChainingHash<String, LinkedList<Servicio>> hashB2;
	private LinearProbingHash<String, BinarySearchTree<String, LinkedList<Servicio>>> hashC2;
	private LinkedList<Servicio> servicios3C;
	private BinarySearchTree<Double , FechaServicios> treeC3;
	public static double referenceLatitude;
	public static double referenceLongitude;
	private LinkedList<Servicio> list1;


	@Override 
	public boolean loadServices(String direccionJson)
	{
		boolean pudeCargar =false;
		list1 = new LinkedList<Servicio>();
		treeA1 = new RedBlackBST<String, LinkedList<String>>();
		hash = new SeparateChainingHash<Integer, LinkedList<Servicio>>(10);

		double totalLatitude = 0;
		double totalServicesWithLatitude = 0;
		double totalLongitude = 0;
		double totalServicesWithLongitude = 0;

		System.out.println("Cargar sistema con" + direccionJson);
		File arch = new File(direccionJson);

		try 
		{
			if(arch.isDirectory()) {
				File[] files = arch.listFiles();
				for(int i = 0; i<files.length; i++) {
					Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
					Servicio[] service = gson.fromJson(new FileReader(files[i].getAbsolutePath()), Servicio[].class);

					for(Servicio s : service) {
						list1.add(s);
						if(s.getPickUpLatitude() != 0) {
							totalLatitude += s.getPickUpLatitude();
							totalServicesWithLatitude++;
						}
						if(s.getPickUpLongitude() != 0) {
							totalLongitude += s.getPickUpLongitude();
							totalServicesWithLongitude++;
						}
					}
				}
			}
			else {
				Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
				Servicio[] service = gson.fromJson(new FileReader(direccionJson), Servicio[].class);
				for(Servicio s : service) 
				{
					list1.add(s);
					if(s.getPickUpLatitude() != 0) {
						totalLatitude += s.getPickUpLatitude();
						totalServicesWithLatitude++;
					}
					if(s.getPickUpLongitude() != 0) {
						totalLongitude += s.getPickUpLongitude();
						totalServicesWithLongitude++;
					}
				}
			}
			
			referenceLatitude = totalLatitude / totalServicesWithLatitude;
			referenceLongitude = totalLongitude / totalServicesWithLongitude;

			System.out.println("el tamaño de la lista de servicios es:" + list1.size());

			MergeSort.sort(list1, new ServiceCompanyComparator(), true);

			String company = list1.get(0).getCompany();
			if(company!=null) 
			{
				LinkedList<String> taxiIds = new LinkedList<String>();
				int i = 0;
				for(i = 0; i < list1.size(); i++) {
					String company1 = list1.get(i).getCompany();
					if(company1 == null) {
						treeA1.put(company, taxiIds);
						break;
					}
					if(company.equals(company1)) 
					{
						taxiIds.add(list1.get(i).getTaxiId());
					}
					else {
						treeA1.put(company, taxiIds);
						company = company1;
						taxiIds = new LinkedList<String>();
						taxiIds.add(list1.get(i).getTaxiId());
					}
				}

				taxiIds = new LinkedList<String>();
				for(int j=i+1;j<list1.size(); j++) {
					taxiIds.add(list1.get(j).getTaxiId());
				}
				treeA1.put("Independent Owner", taxiIds);
			}

			MergeSort.sort(list1, new ServiceAreaComparator(), true);

			//			int area = list1.get(0).getPickUpArea();

			//			if(area!=0) 
			//			{
			LinkedList<Servicio> services = new LinkedList<Servicio>();
			LinkedList<Integer> areas = new LinkedList<Integer>();
			for(int i = 0; i < list1.size(); i++) 
			{
				int area1 = list1.get(i).getPickUpArea();
				areas.add(area1);

			}
			for(int i = 0; i < areas.size(); i++)
			{
				int code = areas.get(i);
				for(int j = 0; j < list1.size(); j++)
				{
					Servicio actual = list1.get(j);
					if(actual.getPickUpArea() == code)
					{
						services.add(actual);
					}
				}
				hash.put(code, services);
			}
			System.out.println("Se cargó req. 1A");

			rangosTablaHash();
			System.out.println("Se cargó req. 2A");
			arbolServiciosPorDistanciaB1();
			System.out.println("Se cargó req. B1");
			hashServiciosPorZonasB2();
			System.out.println("Se cargó req. B2");
			hashArbolesPorDistanciasC2();
			System.out.println("Se cargó req. C2");
			pudeCargar = true;

		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			System.out.println("HUBO UN ERROR AL CARGAR EL ARCHIVO");
			e.printStackTrace();
		}
		return pudeCargar;
	}


	public void serviciosEnRango()
	{
		int maximo = 61;
		rangos = new LinkedList<RangoDuracion>();

		int contador = 0;
		while(contador <list1.size())
		{
			if(rangos.size() == 0)
			{
				rangos.add(new RangoDuracion(String.valueOf(0),String.valueOf(0)));
				rangos.add(new RangoDuracion(String.valueOf(1),String.valueOf(60)));
			}
			for(int i=0; i< rangos.size();i++)
			{
				boolean encontro = false;
				RangoDuracion actual= rangos.get(i);
				LinkedList<Servicio> servicios = new LinkedList<Servicio>();
				for(int j = 0; j< list1.size() && !encontro; j++)
				{
					Servicio servActual= list1.get(j);
					if(actual.estaDentro(String.valueOf(servActual.getTripSeconds())))
					{
						encontro = true;
						servicios.add(servActual);
					}
					actual.agregarServicio(servActual);
				}

				if(!encontro)
				{
					rangos.add(new RangoDuracion(String.valueOf(maximo), String.valueOf(maximo + 59)));
					maximo += 60;
				}
			}
			contador++;
		}
	}

	public void rangosTablaHash()
	{
		hashA2 = new SeparateChainingHash<Integer, LinkedList<Servicio>>();
		serviciosEnRango();

		int rango = 0;
		for (int i = 0; i < rangos.size(); i++)
		{
			RangoDuracion rangoActual = rangos.get(i);
			if(rangoActual.getLista().size() > 0 && Integer.parseInt(rangoActual.getTiempoInicio()) > 0)
			{
				LinkedList<Servicio> listaServ = rangoActual.getLista(); 
				hashA2.put(rango, listaServ);
			}
			rango++;
		}
	}


	private void arbolServiciosPorDistanciaB1() {
		treeB1 = new BinarySearchTree<Double, LinkedList<Servicio>>();
		MergeSort.sort(list1, new ServiceDistanceComparator(), true);

		Node<Servicio> actual = list1.getNode(0);
		if(actual != null) {

			//Recorro la lista por todos los servicios que tengan distancia cero.
			int i=0;
			double distance = actual.getElement().getTripMiles();
			while(actual != null && distance == 0) {
				actual = actual.getNext();
				distance = actual.getElement().getTripMiles();
				i++;
			}

			//En este punto, la distancia ya es diferente de cero.
			LinkedList<Servicio> services = new LinkedList<Servicio>();
			for(; i < list1.size(); i++) {
				double distance1 = list1.get(i).getTripMiles();
				if(distance == distance1) {
					services.add(list1.get(i));
					if(i==list1.size()-1) {
						treeB1.put(distance, services);
					}
				}
				else {
					treeB1.put(distance, services);
					distance = distance1;
					services = new LinkedList<Servicio>();
					services.add(list1.get(i));
					if(i==list1.size()-1) {
						treeB1.put(distance, services);
					}
				}
			}
		}
	}

	private void hashServiciosPorZonasB2() 
	{
		hashB2 = new SeparateChainingHash<String, LinkedList<Servicio>>();
		MergeSort.sort(list1, new ServicePickUpDropOffAreaComparator(), true);

		Node<Servicio> actual = list1.getNode(0);
		if(actual != null) {

			int zonaIni = actual.getElement().getPickUpArea();
			int zonaFin = actual.getElement().getDropOffArea();

			LinkedList<Servicio> services = new LinkedList<Servicio>();
			for(int i = 0; i < list1.size(); i++) {
				int zonaIni1 = list1.get(i).getPickUpArea();
				int zonaFin1 = list1.get(i).getDropOffArea();

				if(zonaIni == zonaIni1 && zonaFin == zonaFin1) {
					services.add(list1.get(i));
					if(i==list1.size()-1) {
						hashB2.put(zonaIni+"-"+zonaFin, services);
					}
				}
				else {
					hashB2.put(zonaIni+"-"+zonaFin, services);
					zonaIni = zonaIni1;
					zonaFin = zonaFin1;
					services = new LinkedList<Servicio>();
					services.add(list1.get(i));
					if(i==list1.size()-1) {
						hashB2.put(zonaIni+"-"+zonaFin, services);
					}
				}

			}
		}

	}

	private static Double toRad(Double value) {
		return value * Math.PI / 180;
	}

	private void hashArbolesPorDistanciasC2() 
	{
		hashC2 = new LinearProbingHash<String, BinarySearchTree<String, LinkedList<Servicio>>>();
		double ini = 0.0;
		double fin = 0.1;
		for(int i = 0; i < 500; i++) 
		{
			String inicial = ((Double)ini).toString();
			String inicial1 = inicial.substring(0, inicial.indexOf(".")+2);
			String finn = ((Double)fin).toString();
			String final1 = finn.substring(0, finn.indexOf(".")+2);
			hashC2.put(inicial1 + "-" + final1, new BinarySearchTree<String, LinkedList<Servicio>>());
			ini += 0.1;
			fin += 0.1;
		}

		for(int i = 0; i<list1.size(); i++) 
		{
			Servicio act = list1.get(i);
			Double harvesineDist = getHarvesineDistance(referenceLatitude, referenceLongitude, act.getPickUpLatitude(), act.getPickUpLongitude());
			String dist = harvesineDist.toString();
			double distReal = Double.parseDouble(dist.substring(0, dist.indexOf(".")+2));

			BinarySearchTree<String, LinkedList<Servicio>> arbol = hashC2.get(distReal+"-"+(distReal+0.1));
			if(arbol != null) {

				//				System.out.println(act.getTaxiId() + "\n\t"+harvesineDist);
				if(arbol.size() == 0 || arbol.get(act.getTaxiId()) == null) {
					LinkedList<Servicio> services = new LinkedList<Servicio>();
					services.add(act);
					arbol.put(act.getTaxiId(), services);
				}
				else {
					arbol.get(act.getTaxiId()).add(act);
				}
			} 
		}
	}

	public static double getHarvesineDistance(double lat1, double lon1, double lat2, double lon2) {
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth in meters
		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance/1609.344;
	}

	@Override
	public ILinkedList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) 
	{
		// TODO Auto-generated method stub
		LinkedList<TaxiConServicios> taxi = new LinkedList<TaxiConServicios>();
		LinkedList<String> taxis = treeA1.get(compania);
		LinkedList<Servicio> servicios = hash.get(zonaInicio);

		int maximo = 0;
		for(int i = 0; i < taxis.size(); i++)
		{
			int contador = 0;
			String actual = taxis.get(i);
			for(int j = 0; j < servicios.size(); j++)
			{
				Servicio actual2 = servicios.get(j);
				if(actual2.getTaxiId().equalsIgnoreCase(actual))
				{
					contador++;
				}
			}
			if(contador >maximo)
			{
				maximo = contador;		
			}
		}

		for(int i = 0; i < taxis.size(); i++)
		{
			int contador2 = 0;
			String actual = taxis.get(i);
			TaxiConServicios taxiServ = new TaxiConServicios(actual, compania);
			for(int j = 0; j < servicios.size(); j++)
			{
				Servicio actual2 = servicios.get(j);
				if(actual2.getTaxiId().equalsIgnoreCase(actual))
				{
					contador2++;
					taxiServ.agregarServicio(actual2);
				}

			}	
			if(contador2==maximo)
			{
				taxi.add(taxiServ);
			}
		}
		return taxi;
	}

	@Override
	public ILinkedList<Servicio> A2ServiciosPorDuracion(int duracion) 
	{
		// TODO Auto-generated method stub
		int numero = duracion/60;
		LinkedList<Servicio> servicios = new LinkedList<Servicio>();
		if(hashA2.get(numero)!=null)
		{
			servicios = hashA2.get(numero);
		}
		return servicios;
	}

	@Override
	public ILinkedList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		// TODO Auto-generated method stub
		LinkedList<Servicio> services = new LinkedList<Servicio>();

		for(Double llave:treeB1.keys()) {
			if(llave > distanciaMinima && llave < distanciaMaxima) {
				LinkedList<Servicio> listaServiciosLlave = treeB1.get(llave);

				for(int i = 0; i < listaServiciosLlave.size(); i++) {
					services.add(listaServiciosLlave.get(i));
				}
			}
		}

		return services;
	}

	@Override
	public ILinkedList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI,
			String fechaF, String horaI, String horaF) {
		// TODO Auto-generated method stub
		LinkedList<Servicio> services = new LinkedList<Servicio>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		try {
			Date inicio = sdf.parse(fechaI + "T" + horaI);
			Date fin = sdf.parse(fechaF + "T" + horaF);

			for(String llave:hashB2.keys()) {
				if(llave.equals(zonaInicio + "-" + zonaFinal)) {
					LinkedList<Servicio> listaServiciosLlave = hashB2.get(llave);

					for(int i = 0; i < listaServiciosLlave.size(); i++) {
						Date fechaIniServ = listaServiciosLlave.get(i).getStartTime();
						Date fechaFinServ = listaServiciosLlave.get(i).getEndTime();
						if(fechaIniServ != null && fechaFinServ != null 
								&& inicio.compareTo(fechaIniServ)<=0 && fin.compareTo(fechaFinServ)>=0)
							services.add(listaServiciosLlave.get(i));
					}
				}
			}
		}
		catch(Exception e) {
			System.out.println("Hubo un error en el parseo de las fechas");
		}

		return services;
	}

	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub

		MergeSort.sort(list1, new ServiceTaxiIdComparator(), true);

		Node<Servicio> actual = list1.getNode(0);
		LinkedList<Taxi> taxis = new LinkedList<Taxi>();
		if(actual != null) {	

			String id = actual.getElement().getTaxiId();
			double totalMillas = 0;
			double totalDinero = 0;
			int totalServicios = 0;
			String compania = actual.getElement().getCompany();
			for(int i = 0; i < list1.size(); i++) {
				Servicio act = list1.get(i);
				String id1 = act.getTaxiId();
				if(id.equals(id1)) {
					if(act.getTripTotal() != 0 && act.getTripMiles() != 0) {
						totalMillas += act.getTripMiles();
						totalDinero += act.getTripTotal();
						totalServicios++;
					}
					if(i==list1.size()-1) {
						TaxiConPuntos aAgregar = new TaxiConPuntos();
						aAgregar.setId(id);
						aAgregar.setCompany(compania);
						aAgregar.setPuntos(totalMillas != 0 ? ((totalDinero/totalMillas)*totalServicios) : 0);
						taxis.add(aAgregar);
					}
				}
				else {
					TaxiConPuntos aAgregar = new TaxiConPuntos();
					aAgregar.setId(id);
					aAgregar.setCompany(compania);
					aAgregar.setPuntos(totalMillas != 0 ? totalDinero/totalMillas : 0);
					taxis.add(aAgregar);
					id = id1;
					compania = act.getCompany();
					totalMillas = 0;
					totalDinero = 0;
					totalServicios = 0;
					if(act.getTripTotal() != 0 && act.getTripMiles() != 0) {
						totalMillas += act.getTripMiles();
						totalDinero += act.getTripTotal();
						totalServicios++;
					}
					if(i==list1.size()-1) {
						TaxiConPuntos aAgregar1 = new TaxiConPuntos();
						aAgregar1.setId(id);
						aAgregar1.setCompany(compania);
						aAgregar1.setPuntos(totalMillas != 0 ? ((totalDinero/totalMillas)*totalServicios) : 0);
						taxis.add(aAgregar1);
					}
				}
			}
		}

		TaxiConPuntos[] taxisOrdenados = new TaxiConPuntos[taxis.size()];
		for(int i = 0; i< taxis.size(); i++) {
			taxisOrdenados[i] = (TaxiConPuntos)taxis.get(i);
		}
		HeapSort.sort(taxisOrdenados, new TaxiPointsComparator(), true);
		return taxisOrdenados;
	}

	@Override
	public ILinkedList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millas) {
		// TODO Auto-generated method stub

		String dist = ((Double)millas).toString();
		double distReal = Double.parseDouble(dist.substring(0, dist.indexOf(".")+2));
		LinkedList<Servicio> services = hashC2.get(distReal + "-" + (distReal+0.1)).get(taxiIDReq2C);
		return services;
	}

	@Override
	public ILinkedList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) 
	{
		// TODO Auto-generated method stub
		LinkedList<Servicio> servicios3C = 	new LinkedList<Servicio>(); 
		int x = 1;
		int max = 15;

		LinkedList<FechaServicios> fechas = new LinkedList<>();
		FechaServicios fecha1 = new FechaServicios(0); 
		fechas.add(fecha1);

		while(max < 15000)
		{
			if(x < 3)
			{
				FechaServicios nuevaFecha1 = new FechaServicios(max);
				x++;
				max += 15;
				fechas.add(nuevaFecha1);
			}
			else
			{
				FechaServicios nuevaFecha2 = new FechaServicios(max);
				x = 0;
				max += 55;
				fechas.add(nuevaFecha2);					
			}
		}

		for(int i = 0; i < list1.size(); i++)
		{
			Servicio servicio = list1.get(i);
			boolean encontro = false;

			while(!encontro)
			{
				for(int j = 0; j < fechas.size();j++)
				{
					FechaServicios fechaActual = fechas.get(j);
					if (fechaActual.seEncuentraEnRango(Math.abs(servicio.getTimeFromStart())));
					{
						encontro = true;
						fechaActual.agregarServicio(servicio);
					}
				}
			}
		}

		for(int z = 0; z < fechas.size(); z++)
		{
			FechaServicios actual2 = fechas.get(z);
			LinkedList<Servicio> lista = (LinkedList<Servicio>) actual2.getServiciosAsociados();
			treeC3.put(actual2.getHoraInicio(), actual2);
		}

		Boolean encontroAux = false;

		//Quitar los : y el . de la hora para leer el número
		String remplazo = hora.replace(":", "");
		String remplazo2 = remplazo.replace(".", "");

		Double aux = Double.parseDouble(remplazo2);

		double val = 0;

		while(!encontroAux)

			for(int k = 0; k < fechas.size(); k++)
			{
				FechaServicios actual3 = fechas.get(k);
				if(actual3.seEncuentraEnRango(aux))
				{
					encontroAux = true; 
					val = actual3.getHoraInicio();
				}
			}

		FechaServicios fechaAux = treeC3.get(val);
		servicios3C = (LinkedList<Servicio>) fechaAux.getServiciosAsociados();

		return servicios3C;
	}
}