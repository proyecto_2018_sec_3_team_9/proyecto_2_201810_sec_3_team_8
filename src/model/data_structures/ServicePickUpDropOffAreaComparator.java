package model.data_structures;

import java.util.Comparator;

import model.vo.Servicio;

public class ServicePickUpDropOffAreaComparator implements Comparator<Servicio> {

	@Override
	public int compare(Servicio o1, Servicio o2) {
		// TODO Auto-generated method stub
		int comparar = o1.getPickUpArea() - o2.getPickUpArea();
		int comparar1 = o1.getDropOffArea() - o2.getDropOffArea();
		int comparar2 = o1.compareByDateTo(o2);

		if(comparar > 0)
		{
			return 1;
		}
		else if(comparar < 0)
		{
			return -1;
		}
		else
		{
			if(comparar1 > 0)
			{
				return 1;
			}
			else if(comparar1 < 0)
			{
				return -1;
			}
			else
			{
				if(comparar2 > 0)
				{
					return 1;
				}
				else if(comparar2 < 0)
				{
					return -1;
				}
				else
				{
					return 0;
				}
			}
		}
	}

}
