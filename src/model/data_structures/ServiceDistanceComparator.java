package model.data_structures;

import java.util.Comparator;

import model.vo.Servicio;

public class ServiceDistanceComparator implements Comparator<Servicio> {
	public int compare(Servicio o1, Servicio o2) {
		// TODO Auto-generated method stub
		return o1.compareByDistance(o2);
	}
}
