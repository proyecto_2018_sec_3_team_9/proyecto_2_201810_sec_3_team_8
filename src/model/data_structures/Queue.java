package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * This class was created in base of the implementation of Queue
 * found on: https://algs4.cs.princeton.edu/13stacks/Queue.java.html
 */
public class Queue<T> implements IQueue<T>, Iterable<T> 
{
	/**
	 * The first node in the queue.
	 */
	private Node<T> first;    

	/**
	 * The last node in the queue.
	 */
	private Node<T> last;     

	/**
	 * Number of elements n in the queue.
	 */
	private int n;

	/**
	 * Initializes an empty queue.
	 */
	public Queue() 
	{
		first = null;
		last  = null;
		n = 0;
	}

	/**
	 * Adds the item to this queue.
	 * @param  item the item to add
	 */
	@Override
	public void enqueue(T item) 
	{
		Node<T> oldlast = last;
		last = new Node<T>(item);
		last.changeNext(null);
		if (isEmpty())
		{
			first = last;
		}
		else        
		{
			oldlast.changeNext(last);
		}
		n++;
	}


	/**
	 * Removes and returns the item on this queue that was least recently added.
	 * @return the item on this queue that was least recently added
	 * @throws NoSuchElementException if this queue is empty
	 */
	@Override
	public T dequeue() 
	{
		if (isEmpty())
		{
			throw new NoSuchElementException("Queue underflow");
		}
		T item = first.getElement();
		first = first.getNext();
		n--;
		if (isEmpty())
		{
			last = null;   // to avoid loitering
		}
		return item;
	}

	/**
	 * Returns true if this queue is empty.
	 * @return true if this queue is empty; false otherwise
	 */
	@Override
	public boolean isEmpty() 
	{
		boolean isEmpty = false;
		if(first == null)
		{
			isEmpty = true;
		}
		return isEmpty;
	}

	/**
	 * Returns the number of items in this stack.
	 * @return the number of items in this stack
	 */
	public int size() 
	{
		return n;
	}


	/**
	 * Returns an iterator that iterates over the items in this queue in FIFO order.
	 *
	 * @return an iterator that iterates over the items in this queue in FIFO order
	 */
	public Iterator<T> iterator()  {
		return new ListIterator<T>(first);  
	}

	// an iterator, doesn't implement remove() since it's optional
	private class ListIterator<T> implements Iterator<T> {
		private Node<T> current;

		public ListIterator(Node<T> first) {
			current = first;
		}

		public boolean hasNext()  { return current != null;                     }
		public void remove()      { throw new UnsupportedOperationException();  }

		public T next() {
			if (!hasNext()) throw new NoSuchElementException();
			T item = current.getElement();
			current = current.getNext(); 
			return item;
		}
	}

}
