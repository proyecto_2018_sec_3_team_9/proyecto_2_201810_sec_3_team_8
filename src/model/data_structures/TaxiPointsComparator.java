package model.data_structures;

import java.util.Comparator;
import model.vo.TaxiConPuntos;

public class TaxiPointsComparator implements Comparator<TaxiConPuntos> {

	@Override
	public int compare(TaxiConPuntos o1, TaxiConPuntos o2) {
		// TODO Auto-generated method stub
		double comparar = o1.getPuntos() - o2.getPuntos();

		if(comparar > 0)
		{
			return 1;
		}
		else if(comparar < 0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
}
