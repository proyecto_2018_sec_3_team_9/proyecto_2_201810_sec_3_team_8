package model.data_structures;

public class LinkedList <T extends Comparable<T>> implements ILinkedList<T> 
{
	//First node
	private Node<T> head;

	//Actual Node
	private Node<T> actual;

	//Size of the list
	private int size;

	public LinkedList() {
		// TODO Auto-generated constructor stub
		head = null;
		actual = null;
		size = 0;
	}

	@Override
	public void add(T element) {
		// TODO Auto-generated method stub
		if (contains(element)) return; 
		if(head == null) {
			head = new Node<T>(element);
			actual = head;
			size = 1;
		}
		else {
			//			System.out.println("Entre");
			Node<T> actual = head;
			while( actual.hasNext()) {
				actual = actual.getNext();
			}
			actual.changeNext(new Node<T>(element));
			size++;
		}

	}

	public boolean contains(T element) 
	{
		// TODO Auto-generated method stub
		if(head!=null) 
		{
			Node<T> act = head;
			while(act != null) 
			{
				if (act != null && act.getElement().compareTo(element) == 0) 
					return true;		
				act = act.getNext();
			}	
		}
		return false;
	}

	@Override
	public boolean remove(T element) {
		// TODO Auto-generated method stub
		if (this.head == null) return false;
		else if (this.head.getElement().equals(element)) {
			this.head = this.head.getNext();
			size--;
			return true;
		}
		else {
			Node<T> actual = head;
			while(actual.hasNext()) {
				Node<T> next = actual.getNext();
				if (next.getElement().compareTo(element) == 0) {
					actual.changeNext(next.getNext());
					size--;
					return true;
				}
				actual = actual.getNext();
			}
		}
		return false;
	}

	@Override
	public T get(T element) {
		// TODO Auto-generated method stub
		if(head != null) {
			Node<T> act = head;
			if(act.getElement().compareTo(element) == 0) return element;
			while(act.hasNext()) {
				act = act.getNext();
				if (act.getElement().compareTo(element) == 0) return element;				
			}	
		}

		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T get(int index) throws IndexOutOfBoundsException{
		if(index >= size) {
			throw new IndexOutOfBoundsException("Size: "+ size + ". Index: " + index);
		}
		else {
			Node<T> actual = head;
			while(index > 0) {
				actual = actual.getNext();
				index = index-1;
			}
			return actual.getElement();
		}
	}

	@Override
	public void listing() {
		// TODO Auto-generated method stub
		actual = head;
	}

	@Override
	public T getCurrent() {
		// TODO Auto-generated method stub
		return actual.getElement();
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		actual = actual.getNext();
		return actual==null?null:actual.getElement();
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		if(actual == null) {
			return false;
		}
		else {
			return actual.getNext() != null;
		}
	}

	@Override
	public void set(T element, int index) throws IndexOutOfBoundsException{
		// TODO Auto-generated method stub
		if(index >= size) {
			throw new IndexOutOfBoundsException("Size: "+ size + ". Index: " + index);
		}
		else {
			Node<T> act = head;
			while(index > 0) {
				act = act.getNext();
				index = index-1;
			}
			act.changeElement(element);
		}
	}

	@Override
	public T previous() {
		//Operation not allowed in this LinkedList
		throw new UnsupportedOperationException("Operation not allowed in simple Linked List");
	}

	@Override
	public boolean hasPrevious() {
		//Operation not allowed in this LinkedList
		throw new UnsupportedOperationException("Operation not allowed in simple Linked List");
	}

	@Override
	public void remove(int index) {
		// TODO Auto-generated method stub
		if(index >= size) {
			throw new IndexOutOfBoundsException("Size: "+ size + ". Index: " + index);
		}
		else if(index == 0) {
			head = head.getNext();
			size--;
		}
		else {
			Node<T> act = head;
			while(index > 0) {
				act = act.getNext();
				index = index-1;
			}
			act = act.getNext();
			size--;
		}
	}

	public Node<T> getNode(int index) throws IndexOutOfBoundsException{
		if(index >= size) {
			throw new IndexOutOfBoundsException("Size: "+ size + ". Index: " + index);
		}
		else {
			Node<T> actual = head;
			while(index > 0) {
				actual = actual.getNext();
				index = index-1;
			}
			return actual;
		}
	}
}

