package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import model.data_structures.ILinkedList;
import model.data_structures.LinkedList;

public class FechaServicios implements Comparable<FechaServicios>
{
	
	private String fecha;
	private ILinkedList<Servicio> serviciosAsociados;
	private int numServicios;
	private double horaInicial;
	private double horaFinal;
	
	public FechaServicios(String pFecha)
	{
		fecha = pFecha;
	}
	
	public FechaServicios(int phoraInicial)
	{
		horaInicial = phoraInicial*100000;
		horaFinal = horaInicial + 1500000;
		serviciosAsociados = new LinkedList<Servicio>();
	}
	public String getFecha() 
	{
		return fecha;
	}
	
	public double getHoraInicio()
	{
		return horaInicial;
	}
	
	public void agregarServicio(Servicio pServicio)
	{
		serviciosAsociados.add(pServicio);
	}
	
	public Boolean seEncuentraEnRango(double pHora)
	{
		Boolean seEncuentra = false;
		if(pHora >= horaInicial && pHora <= horaFinal)
		{
			seEncuentra = true;
		}
		return seEncuentra;
	}
	public void setFecha(String fecha) 
	{
		this.fecha = fecha;
	}
	public ILinkedList<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(ILinkedList<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public int getNumServicios() {
		return numServicios;
	}
	public void setNumServicios(int numServicios) {
		this.numServicios = numServicios;
	}
	@Override
	public int compareTo(FechaServicios o) 
	{
		// TODO Auto-generated method stub
		return fecha.compareToIgnoreCase(o.getFecha());
	}
	
}
