package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	
	/**
	 * Atribute with the tripId of the service.
	 */
	private String trip_id;

	/**
	 * Atribute with the taxiId of the service.
	 */
	private String taxi_id;

	private String company;

	/**
	 * Atribute with the time in seconds of the trip in the service.
	 */
	private int trip_seconds;

	/**
	 * Atribute with the distance in miles of the trip in the service.
	 */
	private double trip_miles;

	/**
	 * Atribute with the total cost of the trip in the service.
	 */
	private double trip_total;

	/**
	 * Atribute with the start time in the service.
	 */
	private Date trip_start_timestamp;

	/**
	 * Atribute with the end time in the service.
	 */
	private Date trip_end_timestamp;

	/**
	 * Atribute with the number of the pick up community area.
	 */
	private int pickup_community_area;

	/**
	 * Atribute with the number of the pick up community area.
	 */
	private int dropoff_community_area;

	private double pickup_centroid_latitude;

	private double pickup_centroid_longitude;

	//	/**
	//	 * Constructor of the service.
	//	 * @param pTripId the id of the trip in the service.
	//	 * @param pTaxiId the taxi id in the service.
	//	 * @param pTripSec time of the service in seconds.
	//	 * @param pTripMiles the distance in miles of the trip of the service.
	//	 * @param pTripTotal the total cost of the trip in the service.
	//	 */
	//	public Servicio(String pTripId, String pTaxiId, int pTripSec, double pTripMiles, double pTripTotal, Date pStartTime, Date pEndTime)
	//	{
	//		trip_id = pTripId;
	//		taxi_id = pTaxiId;
	//		trip_seconds = pTripSec;
	//		trip_miles = pTripMiles;
	//		trip_total = pTripTotal;
	//		trip_start_timestamp = pStartTime;
	//		trip_end_timestamp = pEndTime;
	//	}

	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		// TODO Auto-generated method stub
		return trip_id;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	

	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}	


	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;
	}

	public Date getStartTime()
	{
		return trip_start_timestamp;
	}

	public Date getEndTime()
	{
		return trip_end_timestamp;
	}

	public int getPickUpArea()
	{
		return pickup_community_area;
	}

	public int getDropOffArea()
	{
		return dropoff_community_area;
	}

	public double getPickUpLatitude()
	{
		return pickup_centroid_latitude;
	}

	public double getPickUpLongitude()
	{
		return pickup_centroid_longitude;
	}
	@Override
	public int compareTo(Servicio o) 
	{
		// TODO Auto-generated method stub
		int compare = -1;
		if(trip_id.equals(o.getTripId())){
			compare = 0;
		}
		else if(trip_id.equals(o.getTripId())) {
			return 1;
		}
		return compare;
	}


	public int compareByDateTo(Servicio o) {
		int compare = -1;
		if(trip_start_timestamp.compareTo(o.getStartTime()) == 0){
			compare = 0;
		}
		else if(trip_start_timestamp.compareTo(o.getStartTime()) > 0) {
			return 1;
		}
		return compare;
	}
	public String toString() {
		return trip_id;
	}

	public int compareByDistance(Servicio o)
	{
		int compare = -1;
		if(trip_miles == o.getTripMiles())
		{
			compare = 0;
		}
		else if(trip_miles > o.getTripMiles())
		{
			return 1;
		}
		return compare;
	}
	
	public String getStartHour()
	{
		return getStartTime().toString().substring(11);
	}
	
	public String getFinishHour()
	{
		return getEndTime().toString().substring(11);
	}

	public double getTimeFromStart()
	{
		double rta;
		String remplazo = getStartHour().replace(":", "");
		String remplazofinal = remplazo.replace(".", "");
		rta = Double.parseDouble(remplazofinal);
		return Math.abs(rta);
	}
}

