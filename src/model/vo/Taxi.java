package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	private String taxi_id;
	
	private String company;
	
//	public Taxi(String pTaxiId, String pCompany)
//	{
//		taxi_id = pTaxiId;
//		company = pCompany;
//	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany()
	{
		// TODO Auto-generated method stub
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) 
	{
		// TODO Auto-generated method stub
		int compare = -1;
		if(taxi_id.equals(o.getTaxiId())){
			compare = 0;
		}
		else if(taxi_id.compareTo(o.getTaxiId()) > 0) {
			return 1;
		}
		return compare;
	}

	public void setId(String newId) {
		// TODO Auto-generated method stub
		this.taxi_id = newId;
	}	
	public void setCompany(String newComp) {
		this.company = newComp;
	}
}