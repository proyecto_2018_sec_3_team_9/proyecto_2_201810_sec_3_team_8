package model.vo;

import model.data_structures.LinkedList;

public class RangoDuracion implements Comparable<RangoDuracion> 
{

	//----------------------------------------------
	//Atributos
	//----------------------------------------------

	/**
	 * El tiempo inicial del rango
	 */
	private String tiempoInicial; 

	/**
	 * El tiempo final del rango
	 */
	private String tiempoFinal; 

	/**
	 * Lista de servicios
	 */
	private LinkedList<Servicio> listaServicios;

	//----------------------------------------------
	//Constructor
	//----------------------------------------------

	/**
	 * @param pFechaInicial, fecha inicial del rango
	 * @param pFechaFinal, fecha final del rango
	 * @param pHoraInicio, hora inicial del rango
	 * @param pHoraFinal, hora final del rango
	 */
	public RangoDuracion(String pFechaInicial, String pFechaFinal)
	{
		this.tiempoInicial = pFechaInicial;
		this.tiempoFinal = pFechaFinal;
		listaServicios = new LinkedList<Servicio>();

	}

	//----------------------------------------------
	//Métodos
	//----------------------------------------------

	/**Retorna el tiempo inicial del rango
	 * @return the tiempoInicial
	 */
	public String getTiempoInicio() 
	{
		return tiempoInicial;
	}

	/**Retorna el tiempo final del rango
	 * @return the tiempoInicial
	 */
	public String getTiempoFinal() 
	{
		return tiempoFinal;
	}

	public LinkedList<Servicio> getLista()
	{
		return listaServicios;
	}

	/**
	 * Indica si wl valor dado se encuentra dentro del rango de duracion.
	 * @param pDuracion duracion del rango.
	 * @return true si se encuentra dentro del rango de duracion por parametro.
	 */
	public Boolean estaDentro (String pDuracion)
	{
		Boolean estaDentro = false;
		if(tiempoInicial.compareTo(pDuracion) <= 0 && tiempoFinal.compareTo(pDuracion) >= 0)
		{
			if(Integer.parseInt(tiempoInicial) <= Integer.parseInt(pDuracion) &&  Integer.parseInt(tiempoFinal) >=  Integer.parseInt(pDuracion))
			{
				estaDentro = true;
			}
		}
		return estaDentro;
	}
	
	/**
	 * Agrega un servicio a la lista.
	 */
	public void agregarServicio(Servicio pServicio)
	{
		listaServicios.add(pServicio);
	}
	
	@Override
	public int compareTo(RangoDuracion o) 
	{
		// TODO Auto-generated method stub
		return tiempoInicial.compareToIgnoreCase(o.getTiempoInicio());
	}

	
}
