package model.vo;

import java.text.SimpleDateFormat;

import model.data_structures.ILinkedList;
import model.data_structures.LinkedList;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

    private String taxiId;
    private String compania;
    private ILinkedList<Servicio> servicios;

    public TaxiConServicios(String taxiId, String compania){
        this.taxiId = taxiId;
        this.compania = compania;
        this.servicios = new LinkedList<Servicio>(); // inicializar la lista de servicios 
    }

    public String getTaxiId() {
        return taxiId;
    }

    public String getCompania() {
        return compania;
    }

    public ILinkedList<Servicio> getServicios()
    {
    	return servicios;
    }
    
    public int numeroServicios(){
        return servicios.size();
    }

    public void agregarServicio(Servicio servicio)
    {
        servicios.add(servicio);
    }

    @Override
    public int compareTo(TaxiConServicios o) {
        return taxiId.compareTo( o.getTaxiId());
    }

    public void print(){
        System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        for(int i = 0; i < servicios.size(); i++)
        {
        Servicio s = servicios.get(i);    
        	System.out.println("\t"+sdf.format(s.getStartTime()));
        }
        System.out.println("___________________________________");;
    }
}
