package model.vo;

public class TaxiConPuntos extends Taxi {

	private double puntos;
	/**
     * @return puntos - puntos de un Taxi
     */
	public double getPuntos(){
		return puntos;
	}
	
	public void setPuntos(double puntos) {
		this.puntos = puntos;
	}
}
